$('.navbar-toggle').click(function () {
    if ($('.icon-bar1').hasClass('active')) {
        $('.icon-bar1').removeClass('active');
        $('.icon-bar3').removeClass('active');
        $(".sidenav-overlay").removeClass('active');

        $(".sidenav").css("width", "0");

        setTimeout(function () {
            $('.icon-bar2').removeClass('active');
        }, 300)
    } else {
        $(".sidenav-overlay").addClass('active');

        $(".sidenav").css("width", "80%");
        $('.icon-bar1').addClass('active');
        $('.icon-bar2').addClass('active');
        $('.icon-bar3').addClass('active');
    }
});

$(document).ready(function () {
    $('.header-email').addClass('hide-scroll');
    var scroll_pos = 0;
    $(document).scroll(function () {
        scroll_pos = $(this).scrollTop();
        if (scroll_pos < 250) {
            $('.header-email').addClass('hide-scroll');
        } else {
            $('.header-email').removeClass('hide-scroll');
        }
    });

    $('.header-email').on('mouseover', function () {
        $('.header-email').removeClass('hide-scroll');
    })


    $('.header-email').on('mouseout', function () {
        $('.header-email').addClass('hide-scroll');
    })
});