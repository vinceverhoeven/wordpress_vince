$(document).ready(function () {
    formatVideo();

    $('.grid').masonry({
        columnWidth: 1,
        itemSelector: '.grid-item',
    });

    // le typing
    $(".typed-replace").typed({
        strings: ["Websites ,apps en custom applicaties"],
        typeSpeed: 100,
        backDelay: 2000,
        loop: false
    });
    ///wordpress/wp-content/themes/vince/js/particlesjs-config.json
    if ($('#particles-js').length) {
        particlesJS.load('particles-js', './wp-content/themes/vince/js/particlesjs-config.json', function () {
            console.log('callback - particles.js config loaded');
        });
    }
    $('[data-toggle="tooltip"]').tooltip({container: 'body', placement: 'bottom'});
    $('[data-toggle="tooltip-stappen"]').tooltip({placement: 'top'});

    if (document.getElementById('video')) {
        var video = document.getElementById('video');
        video.play();
        video.loop = true;
    }
});
$(window).resize(function () {
    formatVideo();
});

function formatVideo() {
    var aspectRatio = 1.8;

    var video = $('.video-wrapper video');
    var videoHeight = video.outerHeight() + 30;
    var newWidth = videoHeight * aspectRatio;
    var halfNewWidth = newWidth / 2;

    video.css({"width": newWidth + "px", "left": "50%", "margin-left": "-" + halfNewWidth + "px"});
}


$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    items: 1,
    lazyLoad: true,
    autoplay: true,
    autoplayTimeout: 5000,
    dots: true,
    0: {
        items: 1
    },
    600: {
        items: 1
    },
    1000: {
        items: 1
    }
});
$(function () {
    $(".dropdown").hover(
        function () {
            $(this).addClass('open')
        },
        function () {
            $(this).removeClass('open')
        }
    );
});
// scroll navigation
$('.hero-btn').click(function () {
    $.smoothScroll({scrollTarget: '#block-three-column'});
});
$('.text-overlay .special-button').click(function () {
    $.smoothScroll({
        scrollTarget: '.stappen-description-wrapper',
        offset: -200
    });
});


$(window).scroll(function () {

    $(".widget-programming-in .item").each(function (i) {
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fade it it */
        if (bottom_of_window > bottom_of_object) {
            $(this).delay(200 * i).fadeIn(500);
        }
    });

    $(".circlewrapper .circletag").each(function (i) {
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fade it it */
        if (bottom_of_window > bottom_of_object) {
            $(this).delay(700 * i).fadeIn(500);
        }

    });
});





