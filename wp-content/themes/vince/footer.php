<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vince
 */

?>
<footer id="colophon" class="site-footer color-grey-bg">
    <div class="container">
        <div class="col-md-12  follows">
            <div class="row font-md">
                <p>Let's connect</p>
                <a target="_blank" href="https://www.linkedin.com/in/vinceverhoeven/">
                    <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                </a>
                <a target="_blank" href="https://stackoverflow.com/users/4950828/vince-verhoeven?tab=profile">
                    <i class="fa fa-stack-exchange" aria-hidden="true"></i>
                </a>
            </div>
            <div class="border"></div>
        </div>
        <div class="row site-info">
            <div class="col-md-3 col-sm-3">
				<?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
            </div>
            <div class="col-md-3 col-sm-3">
				<?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>
            </div>
            <div class="col-md-3 col-sm-3">
				<?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
            </div>
            <div class="col-md-3 col-sm-3">
				<?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
            </div>
        </div>
        <div class="row extra-footer">
            <div class="col-md-12">
                <div class="border"></div>
                <div class="font-sm">
					<p><?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'vince' ), 'vince', '<a target="_blank" href="https://www.linkedin.com/in/vinceverhoeven/">Vince Verhoeven</a>' ); ?></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
