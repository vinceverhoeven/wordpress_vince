<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package vince
 */
?>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500');

        body {
            font-family: 'Roboto', sans-serif;
        }

        .font-large {
            font-size: 200px;
        }

        p {
            margin: 0;
            padding: 0;
        }

        .special-button {
            background-color: #ffa92b;
            border-color: #ffbf62;
            color: black;
            border-radius: 0;
            border-bottom-left-radius: 10px;
        }

        .pre-title span {
            font-size: 40px;
            font-weight: 500;
        }

        a {
            text-decoration: none;
            padding: 10px 30px;
            font-size: 20px;
        }

        img {
            margin-top: 15vh;
            padding: 0;
            width: 150px;
            height: auto;
        }

        .font-medium {
            font-size: 30px;
        }
    </style>
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <center>
                <img src="<?php echo get_template_directory_uri() ?>/images/webmango-logo-verticaal.png">
                <p class="font-medium pre-title">
                    <span>Ahh, chips!</span> pagina niet gevonden.</p>
                <p class="font-large title">404</p>
                <a href="http://webmango.nl" class="special-button">Home</a>
            </center>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
