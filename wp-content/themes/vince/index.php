<?php
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main container-fluid">
			<?php get_template_part( 'template-parts/front-page/hero' ); ?>
			<?php get_template_part( 'template-parts/front-page/portfolio' ); ?>
			<?php get_template_part( 'template-parts/front-page/diensten' ); ?>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();