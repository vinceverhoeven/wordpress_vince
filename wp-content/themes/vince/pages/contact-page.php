<?php
/**
 * Template Name: Contact Page
 */
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main contact-page">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-offset-1 col-sm-offset-1 col-md-4 col-sm-5 col-xs-12">
                        <div class="contact-wrapper">
                            <section class="motivation-wrapper">
                                <section class="sense-text-box">
                                    <p class="font-md small-title color-lightgrey-c">Contact</p>
                                    <span class="border"></span>
                                    <p class="font-lg-md big-title color-grey-c">
	                                    <?php if ( get_field( 'title' ) ) {
		                                    echo get_field( 'title' );
	                                    } ?>
                                    </p>
                                    <section class="col-md-9 description">
                                        <div class="font-md color-lightgrey-c ">
	                                        <?php if ( get_field( 'description' ) ) {
		                                        echo get_field( 'description' );
	                                        } ?>
                                        </div>
                                    </section>
                                </section>
                            </section>
	                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
		                        the_content();
	                        endwhile; endif; ?>
                            <section class="contact-info col-md-9 font-md col-xs-12">
                                <p class="telephone">0622624996</p>
                                <a class="email-direct" title="Mail direct" data-container="body" data-toggle="tooltip"
                                   href="mailto:vince@webmango.nl" target="_top">vince@webmango.nl</a>
                            </section>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-6 col-xs-12 full-maps">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d188678.21074548762!2d4.61870823665353!3d51.585752653679144!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c6a029789749ed%3A0x6ff0ea62e6d70e5d!2sGinnekenstraat+112H%2C+4811+JK+Breda!5e0!3m2!1sen!2snl!4v1510089561020"
                                width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php
get_footer();