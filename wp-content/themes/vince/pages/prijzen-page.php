<?php
/**
 * Template Name: Prijzen Page
 */
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main prijzen-page">
            <div class="container">  <!-- me and prices -->
                <div class="row">
                    <section class="motivation-wrapper col-md-offset-3 col-sm-offset-2  col-md-6 col-sm-8">
                        <section class="motivation-wrapper">
                            <section class="sense-text-box">
                                <p class="font-lg-md text-center font-bold">Prijzen<span class="color-grey-c">.</span></p>
                                <div class="border-center"></div>
                                <div class="font-md pretty-text">
									<?php if ( get_field( 'prices-description' ) ) {
										echo get_field( 'prices-description' );
									} ?>
                                </div>
                            </section>
                        </section>
                    </section>
                </div>
                <div class="row prices-titles-wrapper">
                    <div class="col-md-offset-2 col-sm-offset-0 col-md-8 text-center">
                        <div class="col-md-4 col-sm-4">
                            <p class="prices-title font-md-lg font-bold">Uurtarief</p>
                            <div class="font-md">
	                            <?php if ( get_field( 'uurtarief' ) ) {
		                            echo get_field( 'uurtarief' );
	                            } ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <p class="prices-title font-md-lg font-bold">Webhosting</p>
                            <div class="font-md">
		                        <?php if ( get_field( 'webhosting' ) ) {
			                        echo get_field( 'webhosting' );
		                        } ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <p class="prices-title font-md-lg font-bold">Domeinen</p>
                            <div class="font-md">
		                        <?php if ( get_field( 'domeinen' ) ) {
			                        echo get_field( 'domeinen' );
		                        } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	        <?php get_template_part('template-parts/boxes/programming-in'); ?>
            <div class="container-fluid"> <!--Wanna know more -->
                <div class="full-block">
                    <div class="container">
                        <div class="row">
                            <div class="wanna-know-more-block">
                                <div class="col-md-offset-3 col-sm-offset-1 col-md-6 col-sm-10">
                                    <section class="motivation-wrapper">
                                        <section class="sense-text-box">
                                            <p class="font-lg">Wil je meer weten?</p>
                                            <div class="font-md pretty-text">
												<?php dynamic_sidebar( 'wanna-know-more' ); ?>
                                            </div>
											<?php
											$menu = wp_get_nav_menu_items( 'main_nav' );
											$link = vince_get_link_of_menu_name( $menu, "Contact" );
											?>
                                            <a href="<?php echo $link->url ?>">
                                                <button class="btn btn-primary btn-lg cta-btn">
                                                    <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                                    Neem contact op
                                                </button>
                                            </a>
                                        </section>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid"> <!--stappen-->
                <div class="row">
                    <div class="col-md-12">
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php
get_footer();