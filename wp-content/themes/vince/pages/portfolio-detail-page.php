<?php
/**
 * Template Name: Portfolio Detail Page
 * Template Post Type: post, page, product
 */

get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main detail-page">
            <div class="container-fluid">
                <div class="row row-slider">
                    <div class="owl-carousel owl-theme">
						<?php
						$gallery = get_post_gallery_images( $post );
						foreach ( $gallery as $img ) {
							echo "<div class='item'><img src='$img'></div>";
						}
						?>
                    </div>
                </div>
                <div class="row">
                    <section class="col-md-8 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3 col-md-offset-2">
                        <ol class="breadcrumb  color-white-bg">
							<?php
							$menu = wp_get_nav_menu_items( 'main_nav' );
							$link = vince_get_link_of_menu_name( $menu, "Portfolio" );
							?>
                            <li><a href="<?php echo $link->url ?>"><?php echo $link->title ?></a></li>
                            <li class="active"><?php echo $post->post_name ?></li>
                        </ol>
                        <section class="motivation-wrapper">
                            <section class="sense-text-box">
								<?php
								$title = get_the_title( $post );
								?>
                                <p class="font-lg-md text-center"><?php echo $title ?></p>
                                <div class="border-center g"></div>
                                <p class="font-md pretty-text">
									<?php if ( get_field( 'main' ) ) {
										echo get_field( 'main' );
									} ?>
                                </p>
                                <p>Site url:
									<?php if ( get_field( 'site-url' ) ) {
										echo "<a href='http://" . get_field( 'site-url' ) . " ' target='_blank'> " . get_field( 'site-url' ) . "</a>";
									} else {
										echo "<a href='#'> coming soon.</a>";
									} ?>
                                </p>
                            </section>
                        </section>
                    </section>
                </div>
                <div class="detail-page-blocks-wrapper">
                    <section class="detail-page-properties">
                        <div class="row">
                            <section class="info-text">
                                <div class="col-md-offset-2 col-md-4 col-sm-5 col-xs-12 col-sm-offset-1">
                                    <section class="motivation-wrapper">
                                        <section class="sense-text-box">
                                            <p class="font-md-lg">Eigenschappen</p>
                                            <div class="font-md pretty-text stylelist">
												<?php if ( get_field( 'properties' ) ) {
													echo get_field( 'properties' );
												} ?>
                                            </div>
											<?php
											$menu = wp_get_nav_menu_items( 'main_nav' );
											$link = vince_get_link_of_menu_name( $menu, "Contact" );
											?>
                                        </section>
                                    </section>
                                </div>
                            </section>
                            <div class="col-md-5 col-sm-6 col-xs-12">
								<?php if ( get_field( 'properties-image' ) ) {
									$image = get_field( 'properties-image' );
									echo "<img src='{$image['url']}' alt='{$image['alt']}'>";
								} ?>
                            </div>
                        </div>
                    </section>
                    <section class="detail-page-techniques">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-5 col-sm-5 col-xs-12 col-sm-offset-1">
								<?php if ( get_field( 'techniques-image' ) ) {
									$image = get_field( 'techniques-image' );
									echo "<img src='{$image['url']}' alt='{$image['alt']}'>";
								} ?>
                            </div>
                            <section class="info-text">
                                <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-1 col-sm-offset-1">
                                    <section class="motivation-wrapper">
                                        <section class="sense-text-box">
                                            <p class="font-md-lg">Gebruikte technieken</p>
                                            <div class="font-md pretty-text stylelist">
												<?php if ( get_field( 'techniques' ) ) {
													echo get_field( 'techniques' );
												} ?>
                                            </div>
											<?php
											$menu = wp_get_nav_menu_items( 'main_nav' );
											$link = vince_get_link_of_menu_name( $menu, "Contact" );
											?>
                                        </section>
                                    </section>
                                </div>
                            </section>
                        </div>
                    </section>
                </div>
                <div class="row">
                    <div class="wanna-know-more-block">
                        <div class="col-md-offset-3 col-sm-offset-3 col-md-6 col-sm-6">
                            <section class="motivation-wrapper">
                                <section class="sense-text-box">
                                    <p class="font-lg">Wil je meer weten?</p>
                                    <div class="font-md pretty-text">
										<?php dynamic_sidebar( 'wanna-know-more' ); ?>
                                    </div>
									<?php
									$menu = wp_get_nav_menu_items( 'main_nav' );
									$link = vince_get_link_of_menu_name( $menu, "Contact" );
									?>
                                    <a href="<?php echo $link->url ?>">
                                        <button class="btn btn-primary btn-lg cta-btn">
                                            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                            Neem contact op
                                        </button>
                                    </a>
                                </section>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php
get_footer();