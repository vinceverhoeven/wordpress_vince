<?php
/**
 * Template Name: Portfolio Page
 */
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main portfolio-page">
            <div class="container">
                <div class="row">
                    <section class="motivation-wrapper col-md-offset-3 col-sm-offset-2  col-md-6 col-sm-8">
                        <section class="motivation-wrapper">
                            <section class="sense-text-box">
                                <div class="font-lg text-center">
									<?php if ( get_field( 'title' ) ) {
										echo get_field( 'title' );
									} ?>
                                </div>
                                <div class="border-center g"></div>
                                <div class="font-md pretty-text">
									<?php if ( get_field( 'description' ) ) {
										echo get_field( 'description' );
									} ?>
                                </div>
                            </section>
                        </section>
                    </section>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <section class="col-lg-offset-2 col-lg-8 col-md-10 col-md-offset-1 col-sm-12  col-sm-offset-0 col-xs-offset-0 ">
                        <div class="grid">
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
								$columns            = array(
									'htwo col-lg-4 col-md-4 col-sm-4',
									'htwo col-lg-8 col-md-8 col-sm-8',
									'htwo col-lg-7 col-md-7 col-sm-7',
									'htwo col-lg-5 col-md-5 col-sm-5',
									'htwo col-lg-12 col-md-12 col-sm-12',
								);
								$gallery            = get_post_gallery();
								$le_special_gallery = vince_get_gallary_image_src( $gallery );

								foreach ( $le_special_gallery as $key => $value ) {
									?>
                                    <div class="grid-item <?php echo $columns[ $key ] ?> col-xs-12 gradiant">
                                        <a href="<?php echo $value['a'] ?>">
                                            <figure>
                                                <img src="<?php echo $value['src'] ?>" alt="CMS">
                                            </figure>
                                            <div class="image-text-overlay-left">
                                                <p class="font-md-lg special-font color-white-c"><?php echo $value['cap'] ?></p>
                                                <button class="btn btn-warning special-button font-md-sm">Bekijk case
                                                    <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </a>
                                    </div>
									<?php
								}
							endwhile; endif; ?>
                        </div>
                    </section>
                </div>
            </div>

        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();