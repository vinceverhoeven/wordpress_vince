<?php
/**
 * Template Name: Diensten Detail Page
 */

get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main diensten-detail-page">
            <div class="container-fluid header-img-wrapper">
                <div class="container">
                    <div class="col-md-offset-1 col-sm-offset-0 col-sm-12 col-md-7">
                        <div class="text-wrapper">
                            <section class="text-overlay font-lg-md font-bold">
								<?php
								if ( get_the_title( $post ) ) {
									echo get_the_title( $post );
								}
								?>
                                <p class="pretty-text font-md overlay-text-description">
									<?php if ( get_field( 'description-header-overlay' ) ) {
										echo get_field( 'description-header-overlay' );
									} ?>
                                </p>
                                <button class="btn special-button font-md">In 5 stappen
                                    <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                </button>
                            </section>
                        </div>
                    </div>
                </div>
				<?php
				$thumbnail_id = get_post_thumbnail_id( $post->ID );
				$image        = wp_get_attachment_image_src( $thumbnail_id, 'single-post-thumbnail' );
				$alt          = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );
				?>
                <img src="<?php echo $image[0] ?>" alt="<?php echo $alt; ?>"/>
                <div class="img-overlay"></div>
            </div>
            <div class="container z-index-block">
                <div class="row"> <!-- description & privileges -->
                    <div class="col-md-offset-1 col-sm-offset-0 col-sm-12 col-md-6 font-lg-md title-description">
						<?php if ( get_field( 'title-description' ) ) {
							echo get_field( 'title-description' );
						} ?>
                        <div class="border"></div>
                    </div>
                    <div class="col-md-offset-1 col-sm-offset-0 col-sm-12  col-md-6 description">
                        <div class="font-md pretty-text">
							<?php if ( get_field( 'description' ) ) {
								echo get_field( 'description' );
							} ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-offset-1 col-lg-4  col-sm-12 ">
                        <div class="stylelist pretty-text font-md-sm voordelen-block">
                            <p class="pretty-text font-md font-bold">Een paar voordelen</p>
							<?php if ( get_field( 'priveleges' ) ) {
								echo get_field( 'priveleges' );
							} ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="full-block color-acccent-3-bg">
                <div class="container"> <!-- 5 stappen -->
                    <div class="row stappen-description-wrapper">
                        <div class="col-md-offset-1 col-md-11">
                            <p class="font-lg-md">De Webmango werkwijze</p>
                            <div class="border color-acccent-2-bg"></div>
                        </div>
                        <div class="col-md-offset-1  col-md-11">
                            <div class="font-md-lg pretty-text color-grey-c">In 5 stappen.</div>
                        </div>
                    </div>
                    <div class="row circlewrapper">
                        <div class="col-md-offset-1  col-md-2 col-sm-4 col-xs-5 stappen-box">
                            <section class="circletag">
                                <a data-toggle="tooltip-stappen"
                                   title="Laat jouw idee achter. Welke functies, pagina's, huisstijl? Natuurlijk kan dit ook nog even blank zijn."> </a>
                                <img src="<?php echo get_template_directory_uri() ?>/images/idea.png">
                                <p class="text-center stappen-text font-md  font-bold"><span>1</span>  Share your idea</p>
                            </section>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-5 stappen-box">
                            <section class="circletag">
                                <a data-toggle="tooltip-stappen"
                                   title="Heb je een eigen template? Mooi! Dan kan ik die pixel perfect programmeren. Zo niet, dan kan ik een ontwerp realiseren."> </a>
                                <img src="<?php echo get_template_directory_uri() ?>/images/design.png">
                                <p class="text-center stappen-text font-md font-bold"><span>2</span> Eigen of custom ontwerp</p>
                            </section>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-5 stappen-box">
                            <section class="circletag">
                                <a data-toggle="tooltip-stappen"
                                   title="Tijdens de development fase nemen we nauw contact en geef ik updates door."> </a>
                                <img src="<?php echo get_template_directory_uri() ?>/images/coffee.png">
                                <p class="text-center stappen-text font-md font-bold"><span>3</span> Development sprint</p>
                            </section>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-5 stappen-box">
                            <section class="circletag">
                                <a data-toggle="tooltip-stappen"
                                   title="Voor de livegang kun je testen op een afgeschermde url. Alles gecheckt? Dan kan de applicatie live."> </a>
                                <img src="<?php echo get_template_directory_uri() ?>/images/testen.png">
                                <p class="text-center stappen-text font-md font-bold"><span>4</span> testen</p>
                            </section>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-5 stappen-box">
                            <section class="circletag">
                                <a data-toggle="tooltip-stappen"
                                   title="De applicatie staat live! Eventuele bugs los ik natuurlijk op."> </a>
                                <img src="<?php echo get_template_directory_uri() ?>/images/live.png">
                                <p class="text-center stappen-text font-md font-bold"><span>5</span> Live & Support</p>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid block-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="wanna-know-more-block">
                            <div class="col-md-offset-3 col-sm-offset-0 col-md-6 col-sm-">
                                <section class="motivation-wrapper">
                                    <section class="sense-text-box">
                                        <p class="font-lg">Wil je meer weten?</p>
                                        <div class="font-md pretty-text">
											<?php dynamic_sidebar( 'wanna-know-more' ); ?>
                                        </div>
										<?php
										$menu = wp_get_nav_menu_items( 'main_nav' );
										$link = vince_get_link_of_menu_name( $menu, "Contact" );
										?>
                                        <a href="<?php echo $link->url ?>">
                                            <button class="btn btn-primary btn-lg cta-btn">
                                                <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                                Neem contact op
                                            </button>
                                        </a>
                                    </section>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();