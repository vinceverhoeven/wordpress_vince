<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vince
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<?php wp_head(); ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109666937-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-109666937-1');
    </script>
    <meta property="og:site_name" content="Webmango webdevelopment">
    <meta property="og:title" content="Webmango - App of website nodig?"/>
    <meta property="og:type" content="website"/>
    <meta property="og:locale" content="nl_NL" />
</head>

<body <?php
body_class();
?>>
<?php
get_template_part( 'template-parts/header/navbar' );
?>
<div id="page" class="site">
    <div id="content" class="site-content">
