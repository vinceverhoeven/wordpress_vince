var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    order = require('gulp-order'),
    minifyCSS = require('gulp-minify-css'),
    concat = require('gulp-concat');

// Concat Minifies CSS
gulp.task('styles', function () {
    return gulp.src(['./css/vendor/**/*.css', './css/static.css'])
        .pipe(concat('style.css'))
        .pipe(minifyCSS({processImport: false}))
        .pipe(gulp.dest('.'))
});
// Concat Minifies JS
gulp.task('js', function () {
    return gulp.src([
        './js/vendor/*.js',
        './js/*.js'
    ])
        .pipe(uglify())
        .pipe(order([
            "jquery.min.js",
            '*.js'
        ]))
        .pipe(concat('le-gulp-javascript.min.js'))
        .pipe(gulp.dest('./js/dist'))
});

gulp.task('default', function () {
    gulp.run('styles');
    gulp.run('js');
    gulp.watch(['./css/**/*', './js/**/*'], function () {
        gulp.run('styles');
        gulp.run('js');
    })
});
