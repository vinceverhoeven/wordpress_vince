<?php
/**
 * vince functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package vince
 */

if ( ! function_exists( 'vince_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function vince_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on vince, use a find and replace
		 * to change 'vince' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'vince', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'vince' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'vince_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'custom-header', array(
			'width'       => 2000,
			'height'      => 1200,
			'flex-height' => true,
			'video'       => true,
			'video-active-callback' => true
		));

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'vince_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function vince_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'vince_content_width', 640 );
}

add_action( 'after_setup_theme', 'vince_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function vince_widgets_init() {
	// First footer widget area, located in the footer. Empty by default.
	register_sidebar( array(
		'name'          => __( 'First Footer Widget Area', 'widget-vince-footer' ),
		'id'            => 'first-footer-widget-area',
		'description'   => __( 'The first footer widget area', 'tutsplus' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	// Second Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name'          => __( 'Second Footer Widget Area', 'widget-vince-footer' ),
		'id'            => 'second-footer-widget-area',
		'description'   => __( 'The second footer widget area', 'vince' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	// Third Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name'          => __( 'Third Footer Widget Area', 'widget-vince-footer' ),
		'id'            => 'third-footer-widget-area',
		'description'   => __( 'The third footer widget area', 'vince' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	// Fourth Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name'          => __( 'Fourth Footer Widget Area', 'widget-vince-footer' ),
		'id'            => 'fourth-footer-widget-area',
		'description'   => __( 'The fourth footer widget area', 'vince' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'vince' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'vince' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


	register_sidebar( array(
		'name'        => __( 'Fluid Google maps widget', 'widget-vince-fluid-maps' ),
		'id'          => 'widget-vince-fluid-maps',
		'description' => __( 'Fluid Google maps widget', 'vince' ),
	) );

	register_sidebar( array(
		'name'        => __( 'widget-vince-programming-in', 'widget-vince-programming-in' ),
		'id'          => 'widget-vince-programming-in',
		'description' => __( 'Alle talen images', 'vince' ),
	) );

	register_sidebar( array(
		'name'        => __( 'Meer informatie cta', 'wanna-know-more' ),
		'id'          => 'wanna-know-more',
		'description' => __( 'Meer informatie cta', 'vince' ),
	) );
}

add_action( 'widgets_init', 'vince_widgets_init' );

/* =Clean up the WordPress head
------------------------------------------------- */

// remove header links
add_action( 'init', 'vince_head_cleanup' );
function vince_head_cleanup() {
	// REMOVE WP EMOJI
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	remove_action( 'wp_head', 'rsd_link' ); //removes EditURI/RSD (Really Simple Discovery) link.

	remove_action( 'wp_head', 'feed_links_extra', 3 );                      // Category Feeds
	remove_action( 'wp_head', 'feed_links', 2 );                            // Post and Comment Feeds
	remove_action( 'wp_head', 'rsd_link' );                                 // EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' );                         // Windows Live Writer
	remove_action( 'wp_head', 'index_rel_link' );                           // index link
	remove_action( 'wp_head', 'wp_generator' );                             // WP version
	if ( ! is_admin() ) {
		wp_deregister_script( 'jquery' );                                     // De-Register jQuery
	}
}

// remove WP version from RSS
add_filter( 'the_generator', 'tjnz_rss_version' );
function tjnz_rss_version() {
	return '';
}

/**
 * Enqueue scripts and styles.
 */
function vince_scripts() {
	// CSS
	wp_enqueue_style( 'vince-style', get_stylesheet_uri() );

	// JS
	wp_enqueue_script( 'vince-scroll', get_template_directory_uri() . '/js/dist/le-gulp-javascript.min.js',array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'vince_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function vince_check_active_menu( $menu_item ) {
	$actual_link = ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	if ( $actual_link == $menu_item->url ) {
		return 'active';
	}
	if ( $_SERVER['REQUEST_URI'] === '/wordpress/' && $menu_item->title == 'Home' ) {
		return 'active';
	};
	if ( strpos( $actual_link, 'portfolio' ) !== false && $menu_item->title == 'Portfolio' ) {
		return 'active';
	}
	if ( strpos( $actual_link, 'diensten' ) !== false && $menu_item->title == 'Diensten' ) {
		return 'active';
	}

	return '';
}

add_filter( 'template_include', 'vince_default_page_template', 99 );

function vince_default_page_template( $template ) {

	if ( is_singular( 'page' ) ) {
		$default_template = locate_template( array( 'pages/home-page.php' ) );
		if ( '' != $default_template ) {
			return $default_template;
		}
	}

	return $template;
}

function vince_get_youtube_id( $url ) {
	$id = explode( 'watch?v=', $url );

	return $id;
}

function vince_get_gallary_image_src( $content ) {
	$dom = new \DOMDocument;
	libxml_use_internal_errors( true );
	$dom->loadHTML( $content );
	$images = array();

	$anchors = $dom->getElementsByTagName( 'a' );
	$src     = $dom->getElementsByTagName( 'img' );
	$caption = $dom->getElementsByTagName( 'figcaption' );

	foreach ( $src as $key => $value ) {
		$images[ $key ]['src'] = $value->getAttribute( 'src' );
		$images[ $key ]['alt'] = $value->getAttribute( 'alt' );
	}

	foreach ( $anchors as $key => $value ) {
		$images[ $key ]['a'] = $value->getAttribute( 'href' );
	}

	foreach ( $caption as $key => $value ) {
		$images[ $key ]['cap'] = preg_replace( "/\r|\n/", "", $value->nodeValue );
	}

	return $images;
}

function vince_strip_shortcode_gallery( $content ) {
	preg_match_all( '/' . get_shortcode_regex() . '/s', $content, $matches, PREG_SET_ORDER );

	if ( ! empty( $matches ) ) {
		foreach ( $matches as $shortcode ) {
			if ( 'gallery' === $shortcode[2] ) {
				$pos = strpos( $content, $shortcode[0] );
				if ( false !== $pos ) {
					return substr_replace( $content, '', $pos, strlen( $shortcode[0] ) );
				}
			}
		}
	}

	return $content;
}

function vince_get_link_of_menu_name( $menu, $name ) {
	$item_object = array();
	foreach ( $menu as $item ) {
		if ( $item->title == $name ) {
			$item_object = $item;
		}
	}

	return $item_object;
}

function vince_menu_set_dropdown( $sorted_menu_items, $args ) {
	foreach ( $sorted_menu_items as $key => $obj ) {
		// it is a top lv item?
		if ( 0 == $obj->menu_item_parent ) {
			// set the key of the parent
			$last_top = $key;
		} else {
			$sorted_menu_items[ $last_top ]->classes['dropdown'] = 'dropdown';
		}
	}

	return $sorted_menu_items;
}

add_filter( 'wp_get_nav_menu_items', 'vince_menu_set_dropdown', 10, 2 );

function vince_give_parent_children( $id, $menu_items ) {
	$children = array();
	foreach ( $menu_items as $key => $obj ) {
		if ( $menu_items[ $key ]->menu_item_parent == $id ) {
			array_push( $children, $obj );
		}
	}

	return $children;
}

function vince_get_widget_data_for($sidebar_name) {
	global $wp_registered_sidebars, $wp_registered_widgets;

	// Holds the final data to return
	$output = array();

	// Loop over all of the registered sidebars looking for the one with the same name as $sidebar_name
	$sibebar_id = false;
	foreach( $wp_registered_sidebars as $sidebar ) {
		if( $sidebar['name'] == $sidebar_name ) {
			// We now have the Sidebar ID, we can stop our loop and continue.
			$sidebar_id = $sidebar['id'];
			break;
		}
	}

	if( !$sidebar_id ) {
		// There is no sidebar registered with the name provided.
		return $output;
	}


	// A nested array in the format $sidebar_id => array( 'widget_id-1', 'widget_id-2' ... );
	$sidebars_widgets = wp_get_sidebars_widgets();
	$widget_ids = $sidebars_widgets[$sidebar_id];

	if( !$widget_ids ) {
		// Without proper widget_ids we can't continue.
		return array();
	}

	// Loop over each widget_id so we can fetch the data out of the wp_options table.
	foreach( $widget_ids as $id ) {
		// The name of the option in the database is the name of the widget class.
		$option_name = $wp_registered_widgets[$id]['callback'][0]->option_name;

		// Widget data is stored as an associative array. To get the right data we need to get the right key which is stored in $wp_registered_widgets
		$key = $wp_registered_widgets[$id]['params'][0]['number'];

		$widget_data = get_option($option_name);

		// Add the widget data on to the end of the output array.
		$output[] = (object) $widget_data[$key];
	}

	return $output;
}

if (defined('WPSEO_VERSION')) {
	add_action('wp_head',function() { ob_start(function($o) {
		return preg_replace('/^\n?<!--.*?[Y]oast.*?-->\n?$/mi','',$o);
	}); },~PHP_INT_MAX);
}