<div class="container">
    <div class="row">
        <section class="motivation-wrapper col-md-offset-3 col-sm-offset-2  col-md-6 col-sm-8">
            <section class="motivation-wrapper">
                <section class="sense-text-box">
                    <div class="font-lg text-center">
						<?php if ( get_field( 'first-title' ) ) {
							echo get_field( 'first-title' );
						} ?>
                    </div>
                    <div class="border-center"></div>
                    <div class="font-md pretty-text">
						<?php if ( get_field( 'first-description' ) ) {
							echo get_field( 'first-description' );
						} ?>
                    </div>
					<?php
					$menu = wp_get_nav_menu_items( 'main_nav' );
					$link = vince_get_link_of_menu_name( $menu, "Portfolio" );
					?>
                    <a href="<?php echo $link->url ?>" class="cta-sense">Bekijk de portfolio</a>
                </section>
            </section>
        </section>
    </div>
</div>
