<div class="hero-home">
    <div class="row">
        <section class="col-md-12 video-wrapper">
            <div id="particles-js"></div>
			<?php
			if ( get_header_video_url() ) :
				if ( wp_is_mobile() ) : ?>
                    <video loop="" autoplay="" muted="" -webkit-playsinline="" playsinline=""
                           poster="<?php echo get_template_directory_uri() ?>/images/webmang-video-placeholder.jpg"
                           id="video">
                        <source src="<?php echo get_template_directory_uri() ?>/video/webmango-hero-mobile.mp4" type="video/mp4">
                    </video>
				<?php else: ?>
                    <video loop="" autoplay="" muted="" -webkit-playsinline="" playsinline=""
                           poster="<?php echo get_template_directory_uri() ?>/images/webmang-video-placeholder.jpg"
                           id="video">
                        <source src="<?php echo get_header_video_url() ?>" type="video/mp4">
                    </video>
				<?php endif;
			endif; ?>
            <section class="hero-image-text-overlay">
                <div class="container">
                    <div class="col-md-5 col-sm-6 box-hero">
                        <p class="font-lg"><span class="special-font">Online innovatie.</span> <span
                                    class="color-acccent-c typed-replace font-lg-md"></span></p>
                        <button class="btn outline font-md color-acccent-2-c hero-btn"><p class="hero-btn-text">Begin
                                jouw avontuur</p></button>
                    </div>
                </div>
            </section>
        </section>
    </div>
</div>

