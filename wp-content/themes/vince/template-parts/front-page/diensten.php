<section id="block-three-column" class="color-acccent-bg row">
    <div class="container" id="link-services">
        <div class="col-md-12">
            <section class="sense-text-box">
                <p class="font-md small-title color-white-c">Diensten</p>
                <span class="border"></span>
                <p class="font-lg-md big-title color-white-c">Waarmee kan ik je van dienst zijn?</p>
            </section>
        </div>
        <div class="col cornered">
            <section class="col-image">
				<?php if ( get_field( 'dienst-first-column-img' ) ) {
					$img = get_field( 'dienst-first-column-img' );
					echo "<img src='{$img["url"]}' alt='{$img["alt"]}'>";
				} ?>
            </section>
            <section class="col-text">
				<?php
				$menu = wp_get_nav_menu_items( 'main_nav' );
				$link = vince_get_link_of_menu_name( $menu, "WordPress Websites" );
				?>
                <a href="<?php echo $link->url ?>">
                    <circle class="circle col-circle color-acccent-bg color-white-c"></circle>
                </a>
                <h2 class="font-md">Wordpress Websites</h2>
                <p class="font-sm">
					<?php if ( get_field( 'dienst-first-column' ) ) {
						echo get_field( 'dienst-first-column' );
					} ?>
                </p>
                <a href="<?php echo $link->url ?>">Lees meer</a>
            </section>
        </div>
        <div class="col cornered">
            <section class="col-image">
			    <?php if ( get_field( 'dienst-second-column-img' ) ) {
				    $img = get_field( 'dienst-second-column-img' );
				    echo "<img src='{$img["url"]}' alt='{$img["alt"]}'>";
			    } ?>
            </section>
            <section class="col-text">
			    <?php
			    $menu = wp_get_nav_menu_items( 'main_nav' );
			    $link = vince_get_link_of_menu_name( $menu, "Apps" );
			    ?>
                <a href="<?php echo $link->url ?>">
                    <circle class="circle col-circle color-acccent-bg color-white-c"></circle>
                </a>
                <h2 class="font-md">Apps</h2>
                <p class="font-sm">
				    <?php if ( get_field( 'dienst-second-column' ) ) {
					    echo get_field( 'dienst-second-column' );
				    } ?>
                </p>
                <a href="<?php echo $link->url ?>">Lees meer</a>
            </section>
        </div>
        <div class="col cornered">
            <section class="col-image">
				<?php if ( get_field( 'dienst-thirth-column-img' ) ) {
					$img = get_field( 'dienst-thirth-column-img' );
					echo "<img src='{$img["url"]}' alt='{$img["alt"]}'>";
				} ?>
            </section>
            <section class="col-text">
	            <?php
	            $menu = wp_get_nav_menu_items( 'main_nav' );
	            $link = vince_get_link_of_menu_name( $menu, "Custom applicaties" );
	            ?>
                <a href="<?php echo $link->url ?>">
                    <circle class="circle col-circle color-acccent-bg color-white-c"></circle>
                </a>
                <h2 class="font-md">Custom applicaties</h2>
                <p class="font-sm">
					<?php if ( get_field( 'dienst-thirth-column' ) ) {
						echo get_field( 'dienst-thirth-column' );
					} ?>
                </p>
                <a href="<?php echo $link->url ?>">Lees meer</a>
            </section>
        </div>
    </div>
</section>