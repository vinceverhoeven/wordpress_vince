<nav class="navbar navbar-default navbar-fixed-top">
    <section class="header-email color-acccent-2-bg">
        <div class="container">
            <div class="col-md-12 text-right">
                <p class="header-email-text">
                    <span class="prejunct color-grey-c">
                        interesse gekregen?
                    </span>
                    <span class="mail">
                   <a title="Mail direct" data-container="body" data-toggle="tooltip"
                      href="mailto:vince@webmango.nl" target="_top">vince@webmango.nl</a>
                    </span>
                </p>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row content-header">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="icon-bar icon-bar1"></span>
                    <span class="icon-bar icon-bar2"></span>
                    <span class="icon-bar icon-bar3"></span>
                </button>
				<?php
				the_custom_logo();
				?>
            </div>

            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav font-md-sm">
					<?php
					$menu_items = wp_get_nav_menu_items( 'main_nav' );

					foreach ( (array) $menu_items as $key => $menu_item ) {
						if ( ! $menu_item->menu_item_parent ) {
							if ( $menu_item->classes['dropdown'] ) {
								?>
                                <li class="dropdown <?php echo vince_check_active_menu( $menu_item ) ?>">
                                    <a id="drop1" href="#" role="button" class="dropdown-toggle"
                                       data-toggle="dropdown"><?php echo $menu_item->title ?>
                                        <span class="badge color-acccent-2-bg">3</span>
                                        <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
										<?php
										$children = vince_give_parent_children( $menu_item->ID, $menu_items );
										foreach ( $children as $child ) {
											echo "<li><a href='$child->url'>$child->title</a></li>";
										}
										?>
                                    </ul>
                                </li>
								<?php
							} else {
								echo "<li class=" . vince_check_active_menu( $menu_item ) . "><a href='$menu_item->url'>";
								echo $menu_item->title;
								echo "</a></li>";
							}
						}
					}
					?>
                </ul>
            </div>

        </div>
    </div>
</nav>

<!--mobile sidenav-->
<nav class="sidenav">
    <section class="main-sidenav">
        <ul>
			<?php
			$menu_items = wp_get_nav_menu_items( 'main_nav' );
			foreach ( (array) $menu_items as $key => $menu_item ) {
				if ( ! $menu_item->menu_item_parent ) {
					if ( $menu_item->classes['dropdown'] ) {
						?>
                        <a><?php echo $menu_item->title ?>
                            <span class="badge color-acccent-2-bg">3</span>
                            <b class="caret"></b></a>
                        <ul class="side-nav-dropdown">
							<?php
							$children = vince_give_parent_children( $menu_item->ID, $menu_items );
							foreach ( $children as $child ) {
								echo "<li  class=" . vince_check_active_menu( $child ) . "><a href='$child->url'>$child->title</a></li>";
							}
							?>
                        </ul>
						<?php
					} else {
						echo "<li class=" . vince_check_active_menu( $menu_item ) . "><a href='$menu_item->url'>";
						echo $menu_item->title;
						echo "</a></li>";
					}
				}
			}
			?>
        </ul>
    </section>
</nav>
<div class="sidenav-overlay">
</div>
