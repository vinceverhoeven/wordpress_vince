<div class="full-block color-lightgrey-bg">
	<div class="container"> <!--Programming in-->
		<div class="row">
			<div class="col-md-12">
				<section class="widget-programming-in">
					<div class="col-md-12">
						<p class="font-md color-lightgrey-c">Programming in</p>
					</div>
					<?php
					$widgetData = vince_get_widget_data_for( 'widget-vince-programming-in' );

					if ( $widgetData ) {
						foreach ( $widgetData as $key => $value ) {
							?>
							<div class="item col-md-1 col-lg-1 col-sm-1 col-xs-1">
								<a data-container="body" data-toggle="tooltip"
								   title="<?php echo $value->title ?>">
									<img src="<?php echo $value->url ?>" alt="<?php echo $value->alt ?>">
								</a>
							</div>
							<?php
						}
					}
					?>
				</section>
			</div>
		</div>
	</div>
</div>