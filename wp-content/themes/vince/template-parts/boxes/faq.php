<div class="container-fluid block-wrapper">
    <div class="container">
        <div class="row"> <!-- FAQ -->
            <div class="col-md-offset-3 col-sm-offset-0 col-sm-12 col-md-8 font-lg-md">
                <p>Gestelde vragen voor
					<?php
					if ( get_the_title( $post ) ) {
						echo get_the_title( $post );
					}
					?>
                </p>
                <div class="border color-acccent-2-bg"></div>
            </div>
            <div class="col-md-offset-3 col-sm-offset-0 col-sm-12 col-md-8 font-md faq">
				<?php if ( get_field( 'faq' ) ) {
					echo get_field( 'faq' );
				} ?>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid"> <!-- programming in -->
    <div class="row">
        <div class="col-md-12">
        </div>
    </div>
</div>